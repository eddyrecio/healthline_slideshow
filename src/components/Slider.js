import React, {Component} from 'react';
import Slide from './Slide';
import SortBar from './SortBar';
import {createUrl} from '../Utils';

export default class Slider extends Component {

  constructor(props) {
      super(props);
      this.forward = this.forward.bind(this);
      this.back = this.back.bind(this);
      this.onSort = this.onSort.bind(this);
      const slides = this.props.slides;
      this.state = {
        currentIndex: 0,
        count: slides.length,
        slides: slides,
        sortDirection: 'default',
        sortedSlides: slides
      };
  }

  back() {
    let currentIndex = this.state.currentIndex;
    if(currentIndex >0) {
      this.setState({ currentIndex: --currentIndex});
    }
    console.log(this.state.currentIndex);
  }

  forward() {
    let currentIndex = this.state.currentIndex;
    if(currentIndex < this.state.count -1) {
      this.setState({ currentIndex: ++currentIndex});
    }
    console.log(this.state.currentIndex);
  }

  onSort(sortDirection) {
    if(sortDirection=== this.state.sortDirection) return;

    let sortedSlides = sortDirection !== 'alpha'? this.state.slides:
          this.state.slides.concat().sort((a,b)=> {
            if(a.title < b.title) return -1;
            if(a.title > b.title) return 1;
            return 0;
          });

    this.setState({currentIndex: 0, sortDirection: sortDirection, sortedSlides: sortedSlides});
  }

  imagePeek() {
    let currentIndex = this.state.currentIndex;
    if(currentIndex < this.state.count -1) {
      let current = this.state.sortedSlides[++currentIndex];
      (()=> new Image().src = createUrl(current.image.imageUrl))(); //eslint-disable-line no-return-assign
    }
  }

  render() {
    let currentIndex = this.state.currentIndex;
    let current = this.state.sortedSlides[currentIndex];
    let currentSlide = <Slide {...{ title:current.title, body: current.body, ...current.image }} />;

    this.imagePeek();

    return (
      <div>
        <SortBar sort={this.onSort}/>
        { currentSlide }
        <button onClick={this.back} disabled={currentIndex <=0}>
          Previous
        </button>
        <button onClick={this.forward} disabled={currentIndex >= this.state.count -1}>
          Next
        </button>
      </div>
    );
  }
}
