import React, { Component } from 'react';

export default class SortBar extends Component {

  constructor(props) {
    super(props);
    this.onSort = this.onSort.bind(this);
  }

  onSort(e){
    console.log(e.currentTarget.dataset.sort);
    if(this.props.sort) {
      this.props.sort(e.currentTarget.dataset.sort);
    }
  }

  render() {
    return (
      <div className="SortBar">
        Order by:
        <button data-sort="default" onClick={this.onSort}>Default</button>
        <button data-sort="alpha" onClick={this.onSort}>Alpha</button>
      </div>
    );
  }
}
