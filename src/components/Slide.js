import React from 'react';
import {createUrl, trackAnalytics} from '../Utils';

export default function Slide(props) {
  let {imageUrl, imageHeight, imageWidth} = props;

  const imgProps = {
    src: createUrl(imageUrl),
    height: imageHeight,
    width: imageWidth
  };

  trackAnalytics();

  return (
    <div className="Slide">
      <h3>{props.title}</h3>
      <div className="Slide-body">
        <div className="Slide-content"
          dangerouslySetInnerHTML={{__html: props.body}}>
        </div>
        <img {...imgProps} alt={props.title}/>
      </div>
    </div>
  );
}
