import fetchJSONP from 'fetch-jsonp';
import {Component, PropTypes, Children} from 'react';

export default class APIProvider extends Component {

  constructor(props) {
      super(props);
      this.state = { loaded: false};
  }

  componentWillMount() {
    fetchJSONP(this.props.uri)
    .then(response => response.json())
    .then(this.props.onload);
  }

  render() {
    return Children.only(this.props.children);
  }
}

APIProvider.propTypes = {
  uri: PropTypes.string.isRequired,
  onload: PropTypes.func.isRequired
};
