import React, { Component } from 'react';
import './App.css';
import APIProvider from './components/ApiProvider';
import Slider from './components/Slider';

const apiUri = 'https://api.healthline.com/api/service/2.0/slideshow/content?partnerId=7eef498c-f7fa-4f7c-81fd-b1cc53ac7ebc&contentid=17103&includeLang=en';

class App extends Component {

  constructor(props) {
    super(props);
    this.onload = this.onload.bind(this);
    this.state = { isloaded:false};
  }

  onload(json) {
    if(!json && !json.data) {
      throw Error('No data was returned');
    }

    let data = json.data[0];

    this.setState({
      isloaded: !this.state.isloaded,
      title: data.title,
      summary: data.summary,
      slides: data.slides
    });
  }

  render() {
    let content = !this.state.isloaded? <div>Loading...</div>:
      (
      <div className="App">
        <div className="App-header">
          <h2>{this.state.title}</h2>
          <p>{this.state.summary}</p>
        </div>
        <div className="App-body">
          <Slider slides={ this.state.slides } />
        </div>
      </div>);

    return (
      <APIProvider uri={apiUri} onload={ this.onload }>
        {content}
      </APIProvider>
    );
  }
}

export default App;
