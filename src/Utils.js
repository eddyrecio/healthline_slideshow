const rootUrl = 'https://www.healthline.com/';
const clearPix = 'http://www.healthline.com/images/clear.gif';

const timestamp = ()=> Math.floor(Date.now() / 1000);

export function trackAnalytics() {
    (()=> new Image().src = `${clearPix}?${timestamp()}`)(); //eslint-disable-line no-return-assign
}

export const createUrl = (imageUrl) => `${rootUrl}${imageUrl}`;
